# python-package

Template for a simple Python project for [`copier`](https://github.com/copier-org/copier).

## Update template

To update to the underlying meta template, run:

```bash
copier -a .copier-answers.meta-python-project.yml -f update
```

With `-f`, conflicting files are overwritten (which doesn't mean that in the end, the files are changed as those conflicts can be purely internal).
